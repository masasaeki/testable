import { mount } from '@vue/test-utils'
import Menu from '@/components/Menu.vue'
import MenuItem from '@/components/Menu/MenuItem.vue'
import menuItems from '../_mockData/menuItems.json'

describe('Menu.vue', () => {
  const propsData = {
    items: menuItems,
  }
  it('props', () => {
    const wrapper = mount(Menu, {
      propsData,
    })
    expect(wrapper.props()).toEqual(propsData)
  })

  describe('methods', () => {
    it('clickMenuItem', () => {
      const wrapper = mount(Menu, {
        propsData,
      })
      wrapper.vm.onClickMenuItem(menuItems[0])
      expect(wrapper.emitted('clickMenuItem')).toBeTruthy()
      expect(wrapper.emitted('clickMenuItem')[0][0]).toEqual({
        name: menuItems[0].name,
      })
    })
  })

  describe('template', () => {
    it('snapshot', () => {
      const wrapper = mount(Menu, {
        propsData,
      })
      expect(wrapper.vm.$el).toMatchSnapshot()
    })
  })

  it('@clickMenuItem=onClickMenuItem', () => {
    const mock = jest.fn()
    const wrapper = mount(Menu, {
      propsData,
    })
    wrapper.setMethods({
      onClickMenuItem: mock,
    })

    const menuItemAll = wrapper.findAll(MenuItem)
    menuItemAll.at(0).vm.clickMenuItem()

    expect(mock).toHaveBeenCalledWith({ name: menuItems[0].name })

    menuItemAll.at(1).vm.clickMenuItem()

    expect(mock).toHaveBeenCalledWith({ name: menuItems[1].name })
  })
})