
import Vue from 'vue'
import VueRouter from 'vue-router'
import Root from '../pages/Root.vue'
import About from '../pages/About.vue'
import Hoge from '../pages/Hoge.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'root',
    component: Root,
  },
  {
    path: '/about',
    name: 'about',
    component: About,
  },
  {
    path: '/hoge',
    name: 'hoge',
    component: Hoge,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
